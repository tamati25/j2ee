package dbo;

import java.io.Serializable;

public class User implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String username;
	
	private String password;
	
	private Boolean is_admin;
	
	public User()
	{}
	
	public User(String username, String password, Boolean is_admin)
	{
		this.username = username;
		this.password = password;
		this.is_admin = is_admin;
	}

	/* getters & setters */	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIs_admin() {
		return is_admin;
	}

	public void setIs_admin(Boolean is_admin) {
		this.is_admin = is_admin;
	}
}
