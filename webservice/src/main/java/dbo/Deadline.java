package dbo;

import java.io.Serializable;
import java.sql.Date;

public class Deadline implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idDeadline;
	private Date date;
	private float workload;
	private int duration;
	private Lesson lesson;
	
	public Deadline() {}
	
	public Deadline(Date date, float workload, int duration)
	{
		this.date = date;
		this.workload = workload;
		this.duration = duration;
	}

	public int getIdDeadline() {
		return idDeadline;
	}

	public void setIdDeadline(int idDeadline) {
		this.idDeadline = idDeadline;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getWorkload() {
		return workload;
	}

	public void setWorkload(float workload) {
		this.workload = workload;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Lesson getLesson() {
		return lesson;
	}

	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}
}
