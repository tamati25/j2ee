package dbo;

import java.io.Serializable;
import java.util.Set;

public class Lesson implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	private int idLesson;
	private String name;
	private Set<Deadline> deadlines;
	private Set<Tip> tips;
	
	public Lesson() {}
	
	public Lesson(String name)
	{
		this.name = name;

	}

	public int getIdLesson() {
		return idLesson;
	}

	public void setIdLesson(int idLesson) {
		this.idLesson = idLesson;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Deadline> getDeadlines() {
		return deadlines;
	}

	public void setDeadlines(Set<Deadline> deadlines) {
		this.deadlines = deadlines;
	}

	public Set<Tip> getTips() {
		return tips;
	}

	public void setTips(Set<Tip> tips) {
		this.tips = tips;
	}
}
