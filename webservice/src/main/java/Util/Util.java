package Util;

import hibernate.HibernateUtil;

import java.security.MessageDigest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import serialization.*;

public class Util
{
	public static Session session = null;
	
	public static Session getSession()
	{
		if (session == null)
		{
			try
			{
				SessionFactory sf = HibernateUtil.getSessionFactory();
				session = sf.openSession();
				session.beginTransaction();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return session;
	}
	
	public static void endSession()
	{
		if (session != null)
		{
			session.close();
			session = null;
		}
	}
	
	public static String md5(String s)
	{
		String result = "";
		
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] bytes = s.getBytes("UTF-8");
			byte[] digest = md.digest(bytes);
			
			StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < digest.length; i++) {
	          sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
			result = sb.toString();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static String serialize(Object o, String format)
	{
		GenericSerializer serializer;
		
		if (format == null)
			serializer = new XMLSerialization();
		else if (format.equals("xml"))
			serializer = new XMLSerialization();
		else if (format.equals("b64"))
		{
			Log.log("Base64 asked.");
			serializer = new Base64ByteSerialization();
		}
		else if (format.equals("json"))
			serializer = new JSONSerialization();
		else
			serializer = new XMLSerialization();
		
		return serializer.serializeObject(o);
	}
}
