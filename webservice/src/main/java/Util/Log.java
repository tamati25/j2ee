package Util;

public class Log
{
	public static void error(String msg)
	{
		System.err.println("\033[1;31m[ERROR]\033[0;31m " + msg + "\033[0m");
	}
        public static void error(String msg, Exception e)
	{
		System.err.println("\033[1;31m[ERROR]\033[0;31m " + msg + " - " + 
                        e.getClass().getSimpleName() + " : " + e.getMessage() + "\033[0m");
	}
	
	public static void warning(String msg)
	{
		System.out.println("\033[1;33m[WARN]\033[0;33m  " + msg + "\033[0m");
	}
	public static void debug(String msg)
	{
		System.out.println("\033[1;34m[DEBUG]\033[0;34m " + msg + "\033[0m");
	}
        public static void log(String msg)
	{
		System.out.println("Log : " + msg);
	}
}
