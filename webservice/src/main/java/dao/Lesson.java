package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Util.Util;

public class Lesson
{
	/* get all the lessons in the table */
	public static List<dbo.Lesson> getAll()
	{
		List<dbo.Lesson> result = new ArrayList<dbo.Lesson>();
		
		try
		{
			Session session = Util.getSession();

			Query q = session.createQuery("from Lesson");
			result = q.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	/* get lesson by id */
	public static dbo.Lesson getLesson(int id)
	{
		dbo.Lesson result = null;
		try
		{
			Session session = Util.getSession();
			
			result = (dbo.Lesson)session.get(dbo.Lesson.class, id);
		}
		catch (Exception e)
		{
			System.err.println("Error while saving.");
			e.printStackTrace();
		}		

		return result;
	}

	/* Get a user by its name */
	public static dbo.Lesson getLesson(String name)
	{
		dbo.Lesson result = null;
		List<dbo.Lesson> lessons = getAll();
		
		for (dbo.Lesson l : lessons)
		{
			if (l.getName().equals(name))
			{
				result = l;
				break;
			}
		}
		
		return result;
	}

	/* add lesson */
	public static boolean addLesson(String name)
	{
		try
		{
			Session session = Util.getSession();
			dbo.Lesson l = new dbo.Lesson(name);
			int id = (Integer) session.save(l);
			session.getTransaction().commit();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
		// TODO Auto-generated method stub
	}
}
