package dao;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import Util.Util;

public class Tip
{
	public static Set<dbo.Tip> getTips(String lesson)
	{
		Session session = Util.getSession();
		
		HashSet<dbo.Tip> tips = null;
		
		dbo.Lesson l = Lesson.getLesson(lesson);
		
		if (l != null)
		{
			int id = l.getIdLesson();
			
			Query q = session.createQuery("from Tip where idLesson = " + id);
			tips = new HashSet(q.list());
		}		
		return tips;
	}
	
	public static boolean addTip(int id, String tip)
	{
		Session session = Util.getSession();

		dbo.Lesson l = Lesson.getLesson(id);
		
		if (l == null)
			return false;
				
		dbo.Tip newTip = new dbo.Tip(tip, l);
		int thisid = (Integer) session.save(newTip);
		session.getTransaction().commit();
		
		return true;
	}
}
