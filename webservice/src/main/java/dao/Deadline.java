package dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import Util.Util;

public class Deadline
{
	/* Get all the deadlines */
	public static List<dbo.Deadline> getAll()
	{
		List<dbo.Deadline> result = new ArrayList<dbo.Deadline>();
		
		try
		{
			Session session = Util.getSession();
			Query q = session.createQuery("from Deadline");
			result = q.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}

	public static Set<dbo.Deadline> getDeadlines(String lesson)
	{
		Session session = Util.getSession();
		
		HashSet<dbo.Deadline> deadlines = null;
		
		dbo.Lesson l = Lesson.getLesson(lesson);
		
		if (l != null)
		{
			int id = l.getIdLesson();
			
			Query q = session.createQuery("from Deadline where idLesson = " + id);
			deadlines = new HashSet(q.list());
		}		
		return deadlines;
	}
	
	public static boolean addDeadline(int id, String date, float workload, int duration)
	{
		dbo.Lesson l = Lesson.getLesson(id);
		Date sqlDate = Date.valueOf(date); 
		
		if (l == null)
			return false;
		
		Session session = Util.getSession();
		
		dbo.Deadline newDeadline = new dbo.Deadline(sqlDate, workload, duration);
		newDeadline.setLesson(l);
		int thisid = (Integer) session.save(newDeadline);
		session.getTransaction().commit();
		
		return true;
	}
}
