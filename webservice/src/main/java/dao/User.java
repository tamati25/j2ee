package dao;

import hibernate.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Util.Util;

public class User
{
	/* Get all the users */
	public static List<dbo.User> getAll()
	{
		List<dbo.User> result = new ArrayList<dbo.User>();
		
		try
		{
			Session session = Util.getSession();
			Query q = session.createQuery("from User");
			result = q.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	/* Add an user ; return true if successful */
	public static boolean addUser(String name, String password)
	{
		try
		{
			Session session = Util.getSession();

			if (getUser(name) != null)
				return false;
			
			dbo.User u = new dbo.User(name, password, false);
			u.setPassword(Util.md5(password));
			Long id = (Long)session.save(u);
			session.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			System.err.println("Error while saving.");
			e.printStackTrace();
			return false;
		}
	}
	
	/* Get a user by its id */
	public static dbo.User getUser(long id)
	{
		dbo.User result = null;
		try
		{
			Session session = Util.getSession();
			
			result = (dbo.User)session.get(dbo.User.class, id);
		}
		catch (Exception e)
		{
			System.err.println("Error while saving.");
			e.printStackTrace();
		}		

		return result;
	}

	/* Get a user by its name */
	public static dbo.User getUser(String name)
	{
		dbo.User result = null;
		List<dbo.User> users = getAll();
		
		for (dbo.User u : users)
		{
			if (u.getUsername().equals(name))
			{
				result = u;
				break;
			}
		}
		
		return result;
	}

	/* Connect */
	public static boolean connect(String name, String pass)
	{
		dbo.User u = getUser(name);
		
		if (u == null) //User not found in database
			return false;
		
		pass = Util.md5(pass);
		String saved = u.getPassword();
		
		return pass.equals(saved);
	}
}
