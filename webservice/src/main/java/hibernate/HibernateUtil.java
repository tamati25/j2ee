package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil
{	
	private static SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry serviceRegistry; 
	
	private static SessionFactory buildSessionFactory()
	{
		SessionFactory result = null;
		
		try
		{
			Configuration configuration = new Configuration();
			configuration.configure();
			serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
			result = configuration.buildSessionFactory(serviceRegistry);
		}
		catch (Exception e)
		{
			System.err.println("Session factory initialisation failed.");
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
}
