package webService;

import java.util.List;
import java.util.Set;

import javax.jws.WebService;

import Util.Util;
import dao.Deadline;
import dao.Lesson;
import dao.Tip;
import dao.User;

@WebService(serviceName = "HelloWorld", endpointInterface="webService.HelloService" )
public class HelloServiceImpl implements HelloService
{	
	/**
     * This method is only here to test that the webservice is up
     */
    @Override
    public String hello( String txt)
    {
        return "Hello " + txt + " !";
    }

    /**
     * Add an user to the database
     */
    public String adduser(String txt, String password)
    {
    	String result = "";
		
    	if (User.addUser(txt, password) == true)
    		result = "User " + txt + " added successfully !";
    	else
    		result = "Error when adding user " + txt;
    	
    	Util.endSession();
    	return result;
    }

    /**
     * Add a lesson to the database
     */
	@Override
	public String addlesson(String name)
	{
		String result = "";
		
		if (Lesson.addLesson(name) == true)
			result = "Lesson " + name + " added successfully !";
		else
			result = "Error adding lesson " + name;
		
    	Util.endSession();
		return result;
	}

	/*
	 * Add a tip to a lesson
	 */
	@Override
	public String addtip(int lessonId, String tip)
	{
		String result = "";

		if (Tip.addTip(lessonId, tip) == true)
			result = "Tip added, thanks !";
		else
			result = "Failed to add tip, try again.";
		
    	Util.endSession();
		return result;
	}

	/*
	 * Find user by its id
	 */
	@Override
	public String userById(int id, String format)
	{
		String result = "";
		dbo.User user = User.getUser(id);
		
		if (user != null)
		{
			result = Util.serialize(user, format);
		}
		else
			result = "User not found.";
		
    	Util.endSession();
    	return result;
	}

	/* -- Find a user by his name -- */
	@Override
	public String userByName(String name, String format)
	{
		String result = "";
		dbo.User user = User.getUser(name);
		
		if (user != null)
			result = Util.serialize(user, format);
		else
			result = "User not found.";
		
    	Util.endSession();
    	return result;
	}

	/* -- Connect with the credentials supplied -- */
	@Override
	public String connect(String username, String password)
	{
		String result = "";
		boolean ok = User.connect(username, password);
		
		if (ok == true)
		{
			//FIXME : connection ok, do all the things needed ?
			result = "Connection ok";
		}
		else
			result = "Failed to connect. Check your credentials.";

    	Util.endSession();
    	return result;
	}
	
	/* -- Get lesson by its id -- */
	@Override
	public String lessonById(int id, String format)
	{
		String result = "";
		dbo.Lesson lesson = Lesson.getLesson(id);

		if (lesson != null)
			result = Util.serialize(lesson, format);
		else
			result = "Lesson not found.";
		
    	Util.endSession();
    	return result;
	}

	/* -- Get lesson by its name -- */
	@Override
	public String lessonByName(String name, String format)
	{
		String result = "";
		dbo.Lesson lesson = Lesson.getLesson(name);
		
		if (lesson != null)
			result = Util.serialize(lesson, format);
		else
			result = "Lesson not found.";
		
    	Util.endSession();
    	return result;
	}

	/*
	 * Get tips for a lesson, given its name
	 */
	@Override
	public String getTips(String name, String format)
	{
		Set<dbo.Tip> tips = Tip.getTips(name);
		String result = "";
		
		if (tips != null)
		{
			result = Util.serialize(tips, format);
		}
		
    	Util.endSession();
		return result;
	}
	
	/*
	 * Get deadlines for a lesson
	 */
	public String getDeadlines(String name, String format)
	{
		Set<dbo.Deadline> deadlines = Deadline.getDeadlines(name);
		String result = "";
		
		if (deadlines != null)
			result = Util.serialize(deadlines, format);
		
    	Util.endSession();
		return result;
	}
	/*
	 * List all the lessons saved in the database
	 */
	public String listLessons(String format)
	{
		String result = "";
		List<dbo.Lesson> lessons = Lesson.getAll();
		
		if (lessons != null)
			result = Util.serialize(lessons, format);
		
    	Util.endSession();
		return result;
	}
	
	/*
	 * List users in database
	 */
	public String listUsers(String format)
	{
		String result = "";
		List<dbo.User> users = User.getAll();
		
		if (users != null)
			result = Util.serialize(users, format);

    	Util.endSession();
		return result;
	}
	
	/*
	 * List users in database
	 */
	public String listDeadlines(String format)
	{
		String result = "";
		List<dbo.Deadline> deadlines = Deadline.getAll();
		
		if (deadlines != null)
			result = Util.serialize(deadlines, format);
		
    	Util.endSession();
		return result;
	}
	
	
	@Override
	public String addDeadline(int id, String date, float workload, int duration)
	{
		String result = "";
		
		if (Deadline.addDeadline(id, date, workload, duration) == true)
		{
			result = "Added deadline !";
		}
		else
			result = "Failed to add deadline. Try again.";
		
    	Util.endSession();
    	return result;

	}
}