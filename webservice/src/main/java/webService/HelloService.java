package webService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@WebService
@Path("/")
public interface HelloService
{
	/* -- Connect -- */
	@WebMethod
	@POST
	@Path("/connect")
	@Produces("text/html")
	public String connect(@FormParam("username") String username,
			@FormParam("password") String password);
	
	/* -- Get methods -- */
	@WebMethod
	@GET
	@Path("/user/id/{id}")
	@Produces("text/html")
	public String userById(@WebParam(name="name") @PathParam(value="id") int id,
			@WebParam(name="format") @QueryParam(value="format") String format);

	@WebMethod
	@GET
	@Path("/user/name/{name}")
	@Produces("text/html") //FIXME : serializer
	public String userByName(@WebParam(name="name") @PathParam(value="name") String name,
			@WebParam(name="format") @QueryParam(value="format") String format);

	@WebMethod
	@GET
	@Path("/lesson/id/{id}")
	@Produces("text/html")
	public String lessonById(@WebParam(name="name") @PathParam(value="id") int id,
			@WebParam(name="format") @QueryParam(value="format") String format);
	
	@WebMethod
	@GET
	@Path("/lesson/name/{name}")
	@Produces("text/html")
	public String lessonByName(@WebParam(name="name") @PathParam(value="name") String name,
			@WebParam(name="format") @QueryParam(value="format") String format);
	
	@WebMethod
	@GET
	@Path("/lesson/tip/{name}")
	@Produces("text/html")
	public String getTips(@WebParam(name="name") @PathParam(value="name") String name,
			@WebParam(name="format") @QueryParam(value="format") String format);
	
	@WebMethod
	@GET
	@Path("/lesson/deadline/{name}")
	@Produces("text/html")
	public String getDeadlines(@WebParam(name="name") @PathParam(value="name") String name,
			@WebParam(name="format") @QueryParam(value="format") String format);
	
	/* -- list -- */
	@WebMethod
	@GET
	@Path("/user/list")
	public String listUsers(@WebParam(name="format") @QueryParam(value="format") String format);
	
	@WebMethod
	@GET
	@Path("/lesson/list")
	public String listLessons(@WebParam(name="format") @QueryParam(value="format") String format);
	
	@WebMethod
	@GET
	@Path("/deadline/list")
	public String listDeadlines(@WebParam(name="format") @QueryParam(value="format") String format);
	
	/* -- Add methods -- */
	@WebMethod
	@POST
	@Path("/user/add")
	public String adduser(@FormParam("username") String username,
			@FormParam("password") String password);
	
	@WebMethod
	@POST
	@Path("/lesson/add")
	public String addlesson(@FormParam("name") String name);
	
	@WebMethod
	@POST
	@Path("/tip/add")
	@Produces("text/html")
	public String addtip(@FormParam("lessonId") int lessonId,
			@FormParam("tip") String tip);
	
	@WebMethod
	@POST
	@Path("/deadline/add")
	@Produces("text/html")
	public String addDeadline(@FormParam("id") int id,
			@FormParam("date") String date,
			@FormParam("workload") float workload,
			@FormParam("duration") int duration);
	
	/* -- Dummy method -- */
	@WebMethod
	@GET
	@Path("/hello/{txt}")
	public String hello(@WebParam(name="name") @PathParam(value="txt") String txt);

}
