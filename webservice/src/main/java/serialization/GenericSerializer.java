package serialization;

/**
 *
 * @author Eldred
 */
public abstract class GenericSerializer
{
    /**
     * Unserializes an object from a String representing its serialized
     * data.
     * @param chars The serialized object.
     * @return The object converted back to Java object.
     */
    public abstract Object unserializeObject(String chars);
    
    /**
     * Formats the object to a String according to a format depending on
     * the implementing class.
     * @param o
     * @return 
     */
    public abstract String serializeObject(Object o);
}
