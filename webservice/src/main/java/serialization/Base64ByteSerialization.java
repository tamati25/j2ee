/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.apache.commons.codec.binary.Base64;
import Util.Log;

/**
 *
 * @author Eldred
 */
public class Base64ByteSerialization extends GenericSerializer
{
    @Override
    public Object unserializeObject(String chars)
    {
        try
        {
            Base64 b64 = new Base64();
            byte [] data = b64.decode(chars.getBytes());
            ObjectInputStream ois =
                    new ObjectInputStream(new ByteArrayInputStream(data ));
            Object o  = ois.readObject();
            ois.close();
            return o;
        } catch (Exception ex)
        {
            Log.error("Serialization to string failed", ex);
        }
        return null;

    }

    @Override
    public String serializeObject(Object o)
    {
        try
        {
            Base64 b64 = new Base64();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(o);
            oos.close();
            return new String(b64.encode(baos.toByteArray()));
        } catch (Exception ex)
        {
            Log.error("Serialization to string failed", ex);
        }
        return null;
    }    
}
