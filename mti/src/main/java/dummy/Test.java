package dummy;

import java.io.Serializable;

public class Test implements Serializable
{
    private String name;
    private Integer age;
    private Test test;

    public Test getTest()
    {
        return test;
    }

    public void setTest(Test test)
    {
        this.test = test;
    }
        
	public Test()
	{
		this.name = "";
		this.age = 0;
	}
	
	public Test(String name, Integer age)
	{
		this.name = name;
		this.age = age;
	}
	
	/* Getters and setters */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}	
	
}
