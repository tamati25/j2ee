package abstractClasses;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Log;

/**
 *
 * @author bouche_g
 */
public abstract class ContentHolder
{
    public void saveContent()
    {
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(this.getClass().getSimpleName() + ".dump");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
           
            oos.writeObject(this);
        } catch (IOException ex)
        {
            Log.error("Class dictionary could not be saved : " + ex.getMessage());
        } finally
        {
            try
            {
                fos.close();
            } catch (IOException ex) { }
        }
    }
    
    public void LoadContent()
    {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try
        {
            fis = new FileInputStream(this.getClass().getSimpleName() + ".dump");
            ois = new ObjectInputStream(fis);
        } catch (Exception ex)
        {
            Log.error("Load failed", ex);
        } finally
        {
            try
            {
                fis.close();
            } catch (Exception ex)
            {
                Log.error("Load failed", ex);
            }
            try
            {
                ois.close();
            } catch (Exception ex)
            {
                Log.error("Load failed", ex);
            }
        }
    }
}
