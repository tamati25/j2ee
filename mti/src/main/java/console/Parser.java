package console;

import flexjson.JSONSerializer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import util.Log;

public class Parser {
	public static boolean DEBUG_MODE = false;
	
	private static void disp_help() {
		System.out.println("Hibernate auto-mapper");
		System.out.println("List of commands :");
		System.out.println("\tget <class>\tGet a class");
		System.out.println("\tput <class>\tPut a class");
		System.out.println("\tdelete\t\tDelete a previously put class");
		System.out.println("\tlist\t\tGet a list of class");
		System.out.println("\tsync\t\tSynchronize classes");
	}

	private static void put(String cmd) {
		String[] split = cmd.split(" ");

		if (split.length > 1)
		{
			String className = split[1];

			if (DEBUG_MODE == true) //FIXME : if true || true == true && !false then return true.
				Log.debug("put class \033[1m" + className);
			try
			{
				Class<?> c = Class.forName(className);
				Object instance = c.newInstance();
                                
                                // FIXME : faudrait pas plutôt sérialiser la définition de la classe
                                // pour la donner au serveur, plutôt qu'une instance ?
                                // et où sont les actions pour les instances, d'ailleurs ?
				
				/* FIXME : simple serialization using FlexJSON. */
				JSONSerializer serializer = new JSONSerializer().include("*"); 
								
				BufferedWriter writer = new BufferedWriter(new FileWriter("serialized.txt"));
				serializer.deepSerialize(instance, writer);
				writer.close(); /* not very clean. */
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		} else {
			Log.error("Argument error : put <class>");
		}
	}

	private static Object get(String cmd) {
		//fixme
		String[] split = cmd.split(" ");

		if (split.length >= 1)
		{
			String className = split[1];
			
			if (DEBUG_MODE == true)
				Log.debug("get \033[1m" + className);
		}
		else
		{
			Log.error("Argument error : get <class>");
		}
		return new Object();
	}
	
	private static void list()
	{
		if (DEBUG_MODE == true)
			Log.debug("list");
		
		System.out.println("List of classes :");
		System.out.println("  //fixme");
	}
	
	private static void sync()
	{
		if (DEBUG_MODE == true)
			Log.debug("sync");
		
		System.out.println("Synchronizing...");
	}
	
	private static void delete(String cmd)
	{
		//fixme
		String[] split = cmd.split(" ");

		if (split.length >= 1)
		{
			String className = split[1];
			
			if (DEBUG_MODE == true)
				Log.debug("delete \033[1m" + className);
		}
		else
		{
			Log.error("Argument error : delete <class>");
		}
	}
	
	public static void parse(String cmd) {
		String what = "";

		if (cmd == null)
			return;
		
		if (!cmd.equals("")) {
			what = cmd.split(" ")[0];
		}

		// debug only
		if (what.equals("debug")) {
			DEBUG_MODE = (DEBUG_MODE == false ? true : false);
			Log.debug("Debug mode : " + DEBUG_MODE);
		} else if (what.equals("help")) {
			disp_help();
		} else if (what.equals("put")) {
			put(cmd);
		} else if (what.equals("get")) {
			get(cmd);
		} else if (what.equals("list")) {
			list();
		} else if (what.equals("delete")) {
			delete(cmd);
		} else if (what.equals("sync")) {
			sync();
		}
	}
}
