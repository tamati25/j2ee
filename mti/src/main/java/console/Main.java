package console;

import java.io.Console;

public class Main
{
	public static void main (String[] args)
	{
		Console console = System.console();
		
		if (console != null)
		{
			System.out.println("\033[1;32mHibernate auto-mapper\033[0m");
			System.out.println("  Type 'help' to display help");
			System.out.println("  Type 'exit' to quit program.");

			String cmd = "";
			
			while (!cmd.equals("exit"))
			{
				System.out.print("> ");
				
				cmd = console.readLine();
				
				if (cmd == null)
					break;
				
				Parser.parse(cmd);
			}
							
			System.out.println("Exiting.");
		}
	}
}
