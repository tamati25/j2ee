package server.metaprogramming;

import java.lang.reflect.Method;
import server.singletons.ClassDictionary;
import util.Log;

/**
 *
 * @author bouche_g
 */
public class Invoker
{
    private Invoker() { }
    
    public static void ObjectInvokeSetter(Object object,
                                          String fieldName,
                                          Object value) throws InvokerException
    {
        String methodName = makeMethodName("set", fieldName);
        Log.log("Invoker calling method : " + methodName);
        Log.log("With value : " + value.toString());
        
        Class<?> c = ClassDictionary.getInstance().getClass(object.getClass().getSimpleName());
        
        invokeMethod(c, methodName, object, value);
    }
    
    public static Object ObjectInvokeGetter(Object object,
                                            String fieldName) throws InvokerException
    {
        String methodName = makeMethodName("get", fieldName);
        Log.log("Invoker calling method : " + methodName);
        
        Class<?> c = ClassDictionary.getInstance().getClass(object.getClass().getSimpleName());
        
        return invokeMethod(c, methodName, object);
    }
    
    private static String makeMethodName(String prefix, String fieldName)
    {
        StringBuilder NameBuilder = new StringBuilder();
        NameBuilder.append(prefix);
        NameBuilder.append(fieldName);
        NameBuilder.setCharAt(3, Character.toUpperCase(fieldName.charAt(3)));
        
        return NameBuilder.toString();
    }
    
    private static Object invokeMethod(Class<?> c, String methodName, Object o, Object... arg) throws InvokerException
    {
        Method m;
        try
        {
            m = c.getDeclaredMethod(methodName, arg.getClass());
            return m.invoke(o, arg);
        } catch (Exception ex)
        {
            Log.error("InvokeMethod raised an exception", ex);
            throw new InvokerException(ex.getMessage());
        }
        
    }
}
