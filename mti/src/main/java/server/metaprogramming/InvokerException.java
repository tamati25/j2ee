package server.metaprogramming;

/**
 *
 * @author bouche_g
 */
public class InvokerException extends Exception
{
    public final String Message;
    public InvokerException(String msg)
    {
        Message = msg;
    }

}
