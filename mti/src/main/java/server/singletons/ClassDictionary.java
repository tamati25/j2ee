package server.singletons;

import abstractClasses.ContentHolder;
import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Log;

/**
 *
 * @author bouche_g
 */
public class ClassDictionary extends ContentHolder implements Serializable
{
    private static ClassDictionary _instance;
    private HashMap<String, Class<?>> _dict;
    
    private ClassDictionary()
    {
        _dict = new HashMap<String, Class<?>>();
    }
    
    public static ClassDictionary getInstance()
    {
        if (_instance == null)
            _instance = new ClassDictionary();
        return _instance;
    }
    
    public Class<?> getClass(String name)
    {
        return _dict.get(name);
    }
    
    /**
     * Adds a class to the dictionary.
     * @param name The class' name.
     * @param clas The class.
     * @return False if a class with the same name already existed.
     */
    public boolean setClass(String name, Class<?> clas)
    {
        if (_dict.containsKey(name))
            return false;
        _dict.put(name, clas);
        return true;
    }
    
    public Collection<Class<?>> getValues()
    {
        return _dict.values();
    }
    
}
