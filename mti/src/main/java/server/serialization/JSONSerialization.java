package server.serialization;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import util.Log;

/**
 *
 * @author Eldred
 */
public class JSONSerialization extends GenericSerializer
{
    @Override
    public Object unserializeObject(String json)
    {
        JSONDeserializer D = new JSONDeserializer();
        
        return D.deserialize(json);
    }
    
    @Override
    public String serializeObject(Object o)
    {
        JSONSerializer S = new JSONSerializer().include("*").exclude("class");
        
        Log.log("Serialized obj : " + S.deepSerialize(o));
        
        return S.deepSerialize(o);
    }

}
