package server.serialization;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;


/**
 *
 * @author Eldred
 */
public class XMLSerialization extends GenericSerializer
{
    @Override
    public Object unserializeObject(String chars)
    {
        XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(chars.getBytes()));
        
        return decoder.readObject();
    }
    @Override    
    public String serializeObject(Object o)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(baos);
        
        encoder.writeObject(o);
        encoder.flush();
        encoder.close();
        
        return baos.toString();
    }
    
}
