package server.test;

import dummy.Test;
import server.serialization.Base64ByteSerialization;
import server.serialization.GenericSerializer;
import server.serialization.JSONSerialization;
import server.serialization.XMLSerialization;
import util.Log;


/**
 *
 * @author bouche_g
 */
public class TestMain
{
    public static void main(String[] args)
    {
        Test refObj = new Test();
        refObj.setAge(42);
        refObj.setName("quarante deux");

        Test inObj = new Test();
        inObj.setAge(42);
        inObj.setName("quarante deux");

        refObj.setTest(inObj);
        
        GenericSerializer ser = new Base64ByteSerialization();
        
        String serd  = ser.serializeObject(refObj);
        
        Log.log(serd);
        
        Test res = (Test)ser.unserializeObject(serd);
        
        Log.log(res.getName());
    }
}
